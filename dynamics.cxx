#include "Dynamix.hxx"

#define PI 3.14159265358979323846
#define GAMMA_LEN 3

// NOTE: The armadillo linear algebra library is available. Use it!!! :)

// Output: Xdot [gammad, gammadd]
// Inputs: X [gamma, gammad]
//		   U (control inputs)
//		   params (loaded parameters) - access with params["<param name>"]

// Property tree access if needed
PropertyTree *props = PropertyTree::props();

vec Xdot(GAMMA_LEN * 2);

vec dynamics_cxx(vec X, map<string, double> U, map<string, double> params, map<string, itable> tables)
{
	Xdot.zeros();

	for (int i=0; i<GAMMA_LEN; i++) {
		Xdot(i) = X(GAMMA_LEN+i);
	}
	
	// Copy variables: generalized co-ordinates and derivatives
	double x1 = X(0);
	double x2 = X(1);
	double x3 = X(2);
	double x1d = X(3);
	double x2d = X(4);
	double x3d = X(5);

	// Copy parameters
	double m1 = params["m1"];
	double m2 = params["m2"];
	double m3 = params["m2"];
	double k1 = params["k1"];
	double k2 = params["k2"];
	double k3 = params["k2"];
	double b1 = params["b1"];
	double b2 = params["b2"];
	double b3 = params["b2"];

	// Equations of motion
	double x1dd = (k2 * (x2 - x1) + b2 * (x2d - x1d) - k1 * x1 - b1 * x1d) / m1;
	double x2dd = (k3 * (x3 - x2) + b3 * (x3d - x2d) - k2 * (x2 - x1) - b2 * (x2d - x1d)) / m2;
	double x3dd = (U["F"] - k3 * (x3 - x2) - b3 * (x3d - x2d)) / m3;

	// Copy generalized accelerations
	Xdot(3) = x1dd;
	Xdot(4) = x2dd;
	Xdot(5) = x3dd;
	
	return Xdot;
}
