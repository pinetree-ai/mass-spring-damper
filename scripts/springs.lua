l = 0.07

x1 = getprop("/dynamics/gamma/x1")
x2 = getprop("/dynamics/gamma/x2")
x3 = getprop("/dynamics/gamma/x3")

setprop("/animate/spring1", (l + x1) / l - 1)
setprop("/animate/spring2", (l + x2) / l - 1)
setprop("/animate/spring3", (l + x3) / l - 1)