if init then
    t = 0
end

if getprop("/keyboard/state") == 1 then

	key = getprop("/keyboard/key")
    
    -- Go left
	if key == 65361 then
		F = -0.6
    
    -- Go Right
	elseif key == 65363 then
		F = 0.6
	end
else
    F = 0
end

setprop("/animate/F", F-1)
U_F = F

t = t + dt